/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  assetPrefix: process.env.NODE_ENV === "production" ? "/word-game-solver" : "",
  images: {
    loader: "akamai",
    path: "",
  },
};

module.exports = nextConfig;
