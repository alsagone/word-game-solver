import type { NextPage } from "next";
import SearchPage from "../components/searchPage";
import { Jeu } from "../scripts/inputCheck";
const Sutom: NextPage = () => {
  return (
    <div id="app">
      <SearchPage jeu={Jeu.Sutom} />
    </div>
  );
};

export default Sutom;
