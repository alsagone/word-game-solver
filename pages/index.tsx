import type { NextPage } from "next";
import Head from "next/head";
import MyCard from "../components/card";
import { CardGroup } from "semantic-ui-react";
import Image from "next/image";

const Home: NextPage = () => {
  const menuItems = [
    {
      name: "SUTOM",
      path: "/sutom",
      image: "grille-sutom.webp",
      id: "sutom",
    },
    {
      name: "WORDLE FR",
      path: "/wordle-fr",
      image: "grille-wordle-fr.webp",
      id: "wordle-fr",
    },
    {
      name: "WORDLE EN",
      path: "/wordle-en",
      image: "grille-wordle-en.webp",
      id: "wordle-en",
    },
  ];

  const menu = menuItems.map((item) => {
    return (
      <MyCard
        name={item.name}
        path={item.path}
        image={item.image}
        id={item.id}
        key={item.name}
      />
    );
  });

  return (
    <div id="app">
      <h2>Word game solver</h2>
      <div id="home">
        <p>
          Ça fait maintenant 45 minutes que vous êtes devant le WORDLE du jour
          parce que votre cerveau est vide ?<br />
          <br />
          J&quot;ai codé un outil qui vous donne une liste des mots jouables en
          se basant sur les lettres mal placées et celles qui ne sont pas dans
          le mot :)
          <br />
          <br />
          Cliquez sur une des sections ci-dessous pour y accéder.
        </p>
      </div>

      <CardGroup>{menu}</CardGroup>

      <div id="auto-promo">
        <p>
          Site codé par{" "}
          <a
            href="https://twitter.com/alsagone"
            rel="noreferrer norelopener"
            target="_blank"
            title="Mon compte Twitter"
          >
            @alsagone
          </a>
        </p>

        <a
          href="https://ko-fi.com/alsagone"
          target="_blank"
          rel="noreferrer norelopener"
        >
          <Image
            src="/kofi3.webp"
            alt="Buy Me a Coffee at ko-fi.com"
            height={50}
            width={196}
            layout="fixed"
          />
        </a>
      </div>
    </div>
  );
};

export default Home;
