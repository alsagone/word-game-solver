import type { NextPage } from "next";
import SearchPage from "../components/searchPage";
import { Jeu } from "../scripts/inputCheck";
const WordleFR: NextPage = () => {
  return (
    <div id="app">
      <SearchPage jeu={Jeu.WordleFR} />
    </div>
  );
};

export default WordleFR;
