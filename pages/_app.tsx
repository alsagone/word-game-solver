import "semantic-ui-css/semantic.min.css";
import "../styles/card.css";
import "../styles/loading.css";
import "../styles/main-page.css";
import "../styles/navbar.css";
import "../styles/panel.css";
import "../styles/search.css";

import "../styles/style.css";

import type { AppProps } from "next/app";

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default MyApp;
