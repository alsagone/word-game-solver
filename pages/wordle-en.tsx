import type { NextPage } from "next";
import SearchPage from "../components/searchPage";
import { Jeu } from "../scripts/inputCheck";
const WordleEN: NextPage = () => {
  return (
    <div id="app">
      <SearchPage jeu={Jeu.WordleEN} />
    </div>
  );
};

export default WordleEN;
