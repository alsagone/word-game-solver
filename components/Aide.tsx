import React from "react";
import { Modal, Divider, Header, Button } from "semantic-ui-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTwitter, faGitlab } from "@fortawesome/free-brands-svg-icons";

import Image from "next/image";

const Aide = () => {
  const [open, setOpen] = React.useState(false);
  return (
    <Modal
      closeIcon
      open={open}
      trigger={<Button id="bouton-aide">Aide</Button>}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
    >
      <Header icon="help" content="Aide" />
      <Modal.Content>
        <div id="aide">
          <p>Prenons comme exemple cette grille</p>
          <Image
            src="/grille-sutom.webp"
            alt="grille"
            height={302}
            width={352}
            layout="fixed"
          />
          <Divider />

          <p>Fonctionnement des entrées</p>

          <table>
            <thead>
              <tr>
                <th>Ligne</th>
                <th>Modèle</th>
                <th>Lettres jaunes</th>
                <th>Lettres bleues</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>A??????</td>
                <td>O</td>
                <td>MADUE</td>
              </tr>
              <tr>
                <td>2</td>
                <td>A??O???</td>
                <td>NI</td>
                <td>MADUER</td>
              </tr>
            </tbody>
          </table>

          <p>
            La recherche de la 2è ligne vous donne directement &quot;
            <span className="bold">appoint</span>&quot;
            <br />
            <br />
            PS: vous pouvez rentrer vos lettres en majuscule ou en minuscule,
            cela n&apos;a pas d&apos;importance.
          </p>
          <Divider />

          <p>
            Si ça vous intéresse de savoir comment la fonction de filtrage des
            mots fonctionne sous le capot, j&apos;ai écrit{" "}
            <a
              href="sutom_solver.pdf"
              target="_blank"
              rel="noreferrer norelopener"
            >
              ce petit PDF
            </a>{" "}
            où j&apos;explique mon raisonnement :)
          </p>
          <br />

          <div id="credit">
            Bien évidemment, tout le crédit revient à l&apos;équipe de dev
            derrière SUTOM, j&apos;ai juste remarqué que le dictionnaire des
            mots acceptés par le jeu était dans le code JavaScript de la page et
            ça m&apos;a directement donné l&apos;idée de coder ce site :D
          </div>
          <div className="buttons">
            <a
              href="https://twitter.com/alsagone/"
              title="Mon Twitter"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faTwitter} className="icon" />
            </a>

            <a
              href="https://gitlab.com/alsagone/sutom-solver/"
              title="Dépot Git du projet"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faGitlab} className="icon" />
            </a>
          </div>
          <br />
          <span>
            Favicon crée par{" "}
            <a
              href="https://www.flaticon.com/free-icons/words"
              target="_blank"
              rel="noreferrer norelopener"
              title="words icons"
            >
              Freepik
            </a>{" "}
            de{" "}
            <a
              href="https://www.flaticon.com"
              target="_blank"
              rel="noreferrer norelopener"
              title="Flaticon"
            >
              Flaticon
            </a>
          </span>
        </div>
      </Modal.Content>
    </Modal>
  );
};

export default Aide;
