import React from "react";
import { Card } from "semantic-ui-react";
import Link from "next/link";

type Props = {
  name: string;
  path: string;
  image: string;
  id: string;
};

export default class myCard extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Link href={this.props.path} passHref>
        <Card
          id={this.props.id}
          image={this.props.image}
          header={this.props.name}
        />
      </Link>
    );
  }
}
