import React from "react";
import { Form, Input } from "semantic-ui-react";
import Navbar from "./navbar";
import Aide from "./Aide";
import {
  Jeu,
  checkInputLettresExclues,
  checkInputLettresMalPlacees,
  checkInputModele,
  checkInputLongueur,
} from "../scripts/inputCheck";

import { chargerDico } from "../scripts/chargerDico";
import { checkMot, nettoyerMot } from "../scripts/scriptMots";

type Props = {
  jeu: Jeu;
};

class SearchPage extends React.Component<Props> {
  state = {
    longueur: 6,
    minLongueur: 4,
    maxLongueur: 11,
    modele: "",
    placeholder: "",
    lettresMalPlacees: "",
    lettresExclues: "",
    submitModele: "",
    submitLettresMalPlacees: "",
    submitLettresExclues: "",
    erreurInputMalPlacees: "",
    erreurInputLongueur: "",
    erreurInputExclues: "",
    erreurInputModele: "",
    listeMotsJouables: [] as string[],
    dictionnaire: null as Map<number, Map<string, string[]>> | null,
    rechercheVide: false,
    loading: true,
    dictionnaireCharge: false,
  };

  onChangeLongueur: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value: string = e.currentTarget.value;
    const messageErreur: string = checkInputLongueur(value, this.props.jeu);

    this.setState({ erreurInputLongueur: messageErreur });

    if (!messageErreur) {
      const val: number = parseInt(value, 10);
      this.setState({
        longueur: val,
        placeholder: "A" + "?".repeat(val - 1),
      });
    }
  };

  onChangeModele: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value: string = e.currentTarget.value;

    this.setState({
      modele: value,
      erreurInputModele: checkInputModele(
        value,
        this.props.jeu,
        this.state.longueur
      ),
    });
  };

  onChangeMalPlacees: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value: string = e.currentTarget.value;

    this.setState({
      lettresMalPlacees: value,
      erreurInputMalPlacees: checkInputLettresMalPlacees(
        value,
        this.state.modele
      ),
    });
  };

  onChangeExclues: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value: string = e.currentTarget.value;

    this.setState({
      lettresExclues: value,
      erreurInputExclues: checkInputLettresExclues(value),
    });
  };

  handleSubmit = () => {
    const { modele, lettresMalPlacees, lettresExclues } = this.state;
    this.setState(
      {
        submitModele: modele.toLowerCase(),
        submitLettresMalPlacees: lettresMalPlacees.toLowerCase(),
        submitLettresExclues: lettresExclues.toLowerCase(),
      },
      () => this.trouverMots()
    );
  };

  boutonActif = () => {
    const {
      erreurInputExclues,
      erreurInputLongueur,
      erreurInputMalPlacees,
      erreurInputModele,
      dictionnaireCharge,
    } = this.state;
    return (
      dictionnaireCharge &&
      this.state.modele.length !== 0 &&
      [
        erreurInputExclues,
        erreurInputLongueur,
        erreurInputMalPlacees,
        erreurInputModele,
      ].every((err) => !err)
    );
  };

  fetchDico = async (filename: string): Promise<string> => {
    const response = await fetch(`${filename}.txt`).then((res) => {
      return res.text();
    });

    return response;
  };

  getDictionnaire = async () => {
    let filename: string;

    switch (this.props.jeu) {
      case Jeu.WordleEN:
        filename = "dico_wordle_en";
        break;

      case Jeu.WordleFR:
        filename = "dico_wordle_fr";
        break;

      default:
        filename = "dico_sutom";
        break;
    }

    const motsStr: string = await this.fetchDico(filename);
    const motsArr: string[] = motsStr
      .split("\n")
      .map((mot) => nettoyerMot(mot));

    return chargerDico(motsArr);
  };

  estUnMotValide = (mot: string): boolean => {
    const { submitModele, submitLettresMalPlacees, submitLettresExclues } =
      this.state;
    return checkMot(
      mot,
      submitModele,
      submitLettresMalPlacees,
      submitLettresExclues
    );
  };

  trouverMots = () => {
    const { dictionnaire, longueur, submitModele } = this.state;
    const mots: string[] | undefined = dictionnaire
      ?.get(longueur)
      ?.get(submitModele[0]);

    const motsJouables = mots
      ? mots
          .filter((mot) => this.estUnMotValide(mot))
          .sort((a, b) => a.localeCompare(b))
      : [];

    this.setState({
      listeMotsJouables: motsJouables,
      rechercheVide: motsJouables.length === 0,
    });
  };

  constructor(props: Props) {
    super(props);
  }

  async componentDidMount() {
    const dict = await this.getDictionnaire();
    this.setState({
      dictionnaire: dict,
      loading: false,
      dictionnaireCharge: true,
    });

    switch (this.props.jeu) {
      case Jeu.WordleFR:
        this.setState({
          longueur: 5,
          minLongueur: 5,
          maxLongueur: 5,
          placeholder: "A????",
        });
        break;

      case Jeu.WordleEN:
        this.setState({
          longueur: 4,
          minLongueur: 4,
          maxLongueur: 11,
          placeholder: "A???",
        });
        break;

      default:
        this.setState({
          longueur: 6,
          minLongueur: 6,
          maxLongueur: 9,
          placeholder: "A?????",
        });
        break;
    }
  }

  render() {
    const { longueur, lettresMalPlacees, lettresExclues, modele } = this.state;

    const titre = (): React.ReactNode => {
      let jeu: string;

      switch (this.props.jeu) {
        case Jeu.WordleFR:
          jeu = "WORDLE FR";
          break;

        case Jeu.WordleEN:
          jeu = "WORDLE EN";
          break;

        default:
          jeu = "SUTOM";
          break;
      }

      return <h1>{jeu} Solver</h1>;
    };
    const inputLongueur = (): React.ReactNode => {
      return this.props.jeu !== Jeu.WordleFR ? (
        <Form.Field>
          <label>Longueur</label>
          <Form.Input
            type="number"
            name="longueur"
            value={longueur}
            onChange={this.onChangeLongueur}
            min={this.state.minLongueur}
            max={this.state.maxLongueur}
          />
        </Form.Field>
      ) : null;
    };

    const inputModele = (): React.ReactNode => {
      const { erreurInputModele } = this.state;

      return (
        <Form.Field required>
          <label>Modèle</label>
          <Form.Input
            placeholder={this.state.placeholder}
            type="text"
            name="modele"
            value={modele}
            onChange={this.onChangeModele}
            error={erreurInputModele ? erreurInputModele : null}
          />
        </Form.Field>
      );
    };

    const inputMalPlacees = (): React.ReactNode => {
      const { erreurInputMalPlacees } = this.state;

      return (
        <Form.Field>
          <label>Lettres jaunes</label>
          <Form.Input
            placeholder=""
            type="text"
            name="lettresMalPlacees"
            value={lettresMalPlacees}
            error={erreurInputMalPlacees ? erreurInputMalPlacees : null}
            onChange={this.onChangeMalPlacees}
          />
        </Form.Field>
      );
    };

    const inputExclues = (): React.ReactNode => {
      const { erreurInputExclues } = this.state;
      return (
        <Form.Field>
          <label>Lettres bleues</label>
          <Form.Input
            placeholder=""
            type="text"
            name="lettresExclues"
            value={lettresExclues}
            error={erreurInputExclues ? erreurInputExclues : null}
            onChange={this.onChangeExclues}
          />
        </Form.Field>
      );
    };

    const boutonRecherche = (): React.ReactNode => {
      const actif: boolean = this.boutonActif();
      return (
        <Form.Button
          id="bouton-recherche"
          active={actif}
          disabled={!actif}
          primary
          content="Chercher mots jouables"
          onClick={this.handleSubmit}
        />
      );
    };

    const quantificateurLettres = (lettres: string): string => {
      return lettres.length > 1 ? "les lettres" : "la lettre";
    };

    const messageLettresMalPlacees = (): React.ReactNode => {
      const { lettresMalPlacees } = this.state;

      if (!lettresMalPlacees) {
        return null;
      }

      const quantificateur: string = quantificateurLettres(lettresMalPlacees);

      return (
        <li>
          contient {quantificateur} &quot;{lettresMalPlacees}
          &quot;
        </li>
      );
    };

    const messageLettresExclues = (): React.ReactNode => {
      const { lettresExclues } = this.state;

      if (!lettresExclues) {
        return null;
      }

      const quantificateur: string = quantificateurLettres(lettresExclues);

      return (
        <li>
          ne contient pas {quantificateur} &quot;
          {lettresMalPlacees}
          &quot;
        </li>
      );
    };

    const erreurRecherche = (): React.ReactNode => {
      const { longueur, modele } = this.state;
      return (
        <span id="erreurRecherche">
          Aucun mot de {longueur} lettres ne vérifie:
          <ul>
            <li>respecte le modèle &quot;{modele}&quot;</li>
            {messageLettresMalPlacees()}
            {messageLettresExclues()}
          </ul>
        </span>
      );
    };

    const listeMots = (): React.ReactNode => {
      return this.state.rechercheVide ? (
        erreurRecherche()
      ) : (
        <ul className="col-5">
          {this.state.listeMotsJouables.map((mot) => (
            <li key={mot}>{mot}</li>
          ))}
        </ul>
      );
    };

    const ecranChargement = (): React.ReactNode => {
      const { loading, dictionnaireCharge } = this.state;

      if (!loading) {
        return null;
      }

      const message: string = dictionnaireCharge
        ? "Recherche des mots..."
        : "Chargement du dictionnaire...";

      return (
        <div id="loading">
          <div className="sk-folding-cube">
            <div className="sk-cube1 sk-cube"></div>
            <div className="sk-cube2 sk-cube"></div>
            <div className="sk-cube4 sk-cube"></div>
            <div className="sk-cube3 sk-cube"></div>
          </div>
          <span>{message}</span>
        </div>
      );
    };

    return (
      <div id="searchPage">
        {ecranChargement()}

        <Navbar jeu={this.props.jeu} />

        {titre()}
        <Aide />
        <div id="container">
          <div id="inputs">
            <Form autoComplete="off">
              <Form.Group>
                {inputLongueur()}
                {inputModele()}
                {inputMalPlacees()}
                {inputExclues()}
              </Form.Group>
            </Form>
            {boutonRecherche()}
          </div>

          {listeMots()}
        </div>
      </div>
    );
  }
}

export default SearchPage;
