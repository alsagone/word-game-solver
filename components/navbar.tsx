import React from "react";
import { Jeu } from "../scripts/inputCheck";
import Link from "next/link";

import { Menu } from "semantic-ui-react";

const navbarItems = [
  {
    key: "SUTOM",
    href: "/sutom",
    jeu: Jeu.Sutom,
    id: "sutom",
  },
  {
    key: "WORDLE FR",
    href: "/wordle-fr",
    jeu: Jeu.WordleFR,
    id: "wordle-fr",
  },
  {
    key: "WORDLE EN",
    href: "/wordle-en",
    jeu: Jeu.WordleEN,
    id: "wordle-en",
  },
];

type Props = {
  jeu: Jeu;
};

class Navbar extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Menu compact inverted widths={3}>
        {navbarItems.map((item) => (
          <Menu.Item
            key={item.key}
            id={item.id}
            link
            name={item.key}
            href={item.href}
            active={this.props.jeu === item.jeu}
          >
            {item.key}
          </Menu.Item>
        ))}
      </Menu>
    );
  }
}

export default Navbar;
