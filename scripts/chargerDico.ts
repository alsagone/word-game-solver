export const chargerDico = (
  listeMots: string[]
): Map<number, Map<string, string[]>> => {
  let dico: Map<number, Map<string, string[]>> = new Map<
      number,
      Map<string, string[]>
    >(),
    strList: string[] | undefined,
    map: Map<string, string[]> | undefined;

  for (const mot of listeMots) {
    map = dico.get(mot.length);

    if (map) {
      strList = map.get(mot[0]);
      strList = strList ? [...strList, mot] : [mot];
      map.set(mot[0], strList);
    } else {
      map = new Map<string, string[]>();
      map.set(mot[0], [mot]);
      dico.set(mot.length, map);
    }
  }

  return dico;
};
