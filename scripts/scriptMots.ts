const removeCharAt = (str: string, index: number): string => {
  return str.slice(0, index) + str.slice(index + 1);
};

export const respecteModele = (mot: string, modele: string): boolean => {
  if (mot.length !== modele.length) {
    return false;
  }

  let checkModele: boolean = true,
    i: number;

  for (i = 0; i < mot.length; i++) {
    if (modele[i] !== "?" && modele[i] !== mot[i]) {
      checkModele = false;
      break;
    }
  }

  return checkModele;
};

export const checkMot = (
  mot: string,
  modele: string,
  lettresMalPlacees: string,
  lettresExclues: string
): boolean => {
  if (!respecteModele(mot, modele)) {
    return false;
  }

  let checkLettresExclues: boolean = true,
    i: number,
    index: number,
    lettre: string;

  for (i = 0; i < modele.length; i++) {
    if (modele[i] === "?") {
      lettre = mot[i];
      index = lettresMalPlacees.indexOf(lettre);

      if (index >= 0) {
        lettresMalPlacees = removeCharAt(lettresMalPlacees, index);
      } else if (lettresExclues.includes(lettre)) {
        checkLettresExclues = false;
        break;
      }
    }
  }

  return checkLettresExclues && lettresMalPlacees.length === 0;
};

export const nettoyerMot = (mot: string): string => {
  return mot.toLowerCase().replace(/\n|\r/g, "");
};
