/*
WordleFR a constamment des mots de 5 lettres
*/

export enum Jeu {
  WordleFR = "WordleFR",
  WordleEN = "WordleEN",
  Sutom = "Sutom",
}

const regexModeleWordle: RegExp = /^[A-Za-z?]*$/;
const regexLettres: RegExp = /^[A-Za-z]*$/;
const regexModeleSutom: RegExp = /^[A-Za-z][A-Za-z?]*$/;

const nombreLettresInconnues = (modele: string): number => {
  return [...modele].filter((lettre) => lettre === "?").length;
};

const checkRegexModele = (modele: string, jeu: Jeu): boolean => {
  return jeu === Jeu.Sutom
    ? regexModeleSutom.test(modele)
    : regexModeleWordle.test(modele);
};

export const checkInputModele = (
  modele: string,
  jeu: Jeu,
  longueurMot: number
): string => {
  if (modele.length === 0) {
    return "Modèle requis";
  }

  if (modele.startsWith("?")) {
    return "Le modèle doit commencer par une lettre.";
  }

  if (!checkRegexModele(modele, jeu)) {
    return "N'entrer que des lettres ou des ?";
  }

  return modele.length !== longueurMot
    ? `Le modèle doit faire exactement ${longueurMot} caractères`
    : "";
};

export const checkInputLettresMalPlacees = (
  lettresMalPlacees: string,
  modele: string
): string => {
  if (!lettresMalPlacees) {
    return "";
  }

  if (!regexLettres.test(lettresMalPlacees)) {
    return "N'entrer que des lettres.";
  }

  return lettresMalPlacees.length > nombreLettresInconnues(modele)
    ? "Trop de lettres mal placées par rapport au nombre de '?' dans le modèle"
    : "";
};

export const checkInputLettresExclues = (lettresExclues: string): string => {
  return lettresExclues && !regexLettres.test(lettresExclues)
    ? "N'entrer que des lettres"
    : "";
};

export const checkInputLongueur = (inputLongueur: string, jeu: Jeu): string => {
  //Check si l'input est bien un nombre
  if (!/^-?\d+$/.test(inputLongueur)) {
    return "Entrer un nombre";
  }

  let min: number = 4,
    max: number = 11;

  if (jeu === Jeu.Sutom) {
    min = 6;
    max = 9;
  }

  const messageErreur: string = `La longueur doit être comprise entre ${min} et ${max}.`;
  const longueur: number = parseInt(inputLongueur, 10);

  return min <= longueur && longueur <= max ? "" : messageErreur;
};
